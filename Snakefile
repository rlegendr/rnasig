#########################################################################
# RNAsig: an automated pipeline to detect RNA signatures on viruses     #
#                                                                       #
# Authors: Rachel Legendre                                              #
# Copyright (c) 2020-2021  Institut Pasteur (Paris).                    #
#                                                                       #
# This file is part of RNAsig workflow.                                 #
#                                                                       #
# RNAsig is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# RNAsig is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details .                         #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with RNAsig (LICENSE).                                          #
# If not, see <https://www.gnu.org/licenses/>.                          #
#########################################################################


import pandas as pd
from fnmatch import fnmatch
from re import sub, match
from itertools import repeat, chain
import os


#-------------------------------------------------------
# read config files

configfile: "config/config.yaml"
RULES = os.path.join("workflow", "rules")
design = pd.read_csv(config["design"]["design_file"], header=0, sep='\t')


#-------------------------------------------------------
# list of all files in the directory 'input_dir'

filenames = [f for f in os.listdir(config["input_dir"]) if match(r'.*'+config["input_mate"]+config["input_extension"]+'', f)] 
samples = [sub(config["input_mate"]+config["input_extension"], '', file) for file in filenames]
receptor = [ x.strip() for x in (config["design"]["receptor"]).split(",")]
conds = [ x.strip() for x in (config["design"]["condition"]).split(",")]


#-------------------------------------------------------
# paired-end data gestion
mate_pair = config["input_mate"]
rt1 = mate_pair.replace("[12]", "1")
rt2 = mate_pair.replace("[12]", "2")

R1 = [1 for this in filenames if rt1 in this]
R2 = [1 for this in filenames if rt2 in this]

if len(R2) == 0:
    paired = False
else:
    if R1 == R2:
        paired = True
    else:
        raise ValueError("Please provides single or paired files only")



# -----------------------------------------------
#initialize global variables


sample_dir = config["input_dir"]
analysis_dir = config["analysis_dir"]

input_data = [sample_dir + "/{{SAMPLE}}{}.fastq.gz".format(rt1)]
if paired:
    input_data += [sample_dir + "/{{SAMPLE}}{}.fastq.gz".format(rt2)]    
# global variable for all output files
final_output = []


# -----------------------------------------------
#begin of the rules

#----------------------------------
# quality control
#----------------------------------

fastqc_input_fastq = input_data
fastqc_output_done = "00-Fastqc/{SAMPLE}_R1_fastqc.fastq.gz"
fastqc_wkdir = "00-Fastqc"
fastqc_log = "00-Fastqc/logs/{SAMPLE}_R1_fastqc_raw.log"
final_output.extend(expand(fastqc_output_done, SAMPLE=samples))

include: os.path.join(RULES, "fastqc.rules")


#----------------------------------
# remove adapters
#----------------------------------

if config["adapters"]["remove"] :

    ## TODO add AlienTrimmer
    adapter_tool = "adapters"
    adapters_input_fastq = input_data
    adapters_wkdir = "01-Trimming"
    adapters_output = ["01-Trimming/{SAMPLE}_R1_trim.fastq.gz"]
    if paired:
        adapters_output += ["01-Trimming/{SAMPLE}_R2_trim.fastq.gz"]

    # Set parameters
    adapters_adapt_list = config["adapters"]["adapter_list"]
    adapters_options = config["adapters"]["options"]
    adapters_mode = config["adapters"]["mode"]
    adapters_min  = config["adapters"]["m"]
    adapters_qual = config["adapters"]["quality"]
    adapters_log = "01-Trimming/logs/{SAMPLE}_trim.txt"
    final_output.extend(expand(adapters_output, SAMPLE=samples))
    include: os.path.join(RULES, "adapters.rules")

else:
    adapters_output = input_data
    
    

#----------------------------------
# genome gestion
#----------------------------------

ref = [ config["genome"]["name"] ]

if config["genome"]["host_mapping"]:
    ref += [ config["genome"]["host_name"] ]


def mapping_index(wildcards):
    if (wildcards.REF == config["genome"]["name"]):
        input = config["genome"]["fasta_file"]
    elif (wildcards.REF == config["genome"]["host_name"]):
        input = config["genome"]["host_fasta_file"] 
    return(input)    
    
    


#----------------------------------
# bowtie2 MAPPING
#----------------------------------


# indexing for bowtie2
bowtie2_index_fasta = mapping_index
bowtie2_index_output_done = os.path.join(config["genome"]["genome_directory"],"{REF}.1.bt2")
bowtie2_index_output_prefix = os.path.join(config["genome"]["genome_directory"],"{REF}")
bowtie2_index_log = "02-Mapping/bowtie2/logs/bowtie2_{REF}_indexing.log"
include: os.path.join(RULES, "bowtie2_index.rules")



# Mapping step
bowtie2_mapping_input = adapters_output
bowtie2_mapping_index_done = bowtie2_index_output_done
bowtie2_mapping_sort = "02-Mapping/{REF}/{SAMPLE}_{REF}_sort.bam"
bowtie2_mapping_bam = "02-Mapping/{REF}/{SAMPLE}_{REF}.bam"
bowtie2_mapping_sortprefix = "02-Mapping/{REF}/{SAMPLE}_{REF}_sort"
bowtie2_mapping_logs_err = "02-Mapping/{REF}/logs/{SAMPLE}_{REF}_mapping.err"
bowtie2_mapping_logs_out = "02-Mapping/{REF}/logs/{SAMPLE}_{REF}_mapping.out"
bowtie2_mapping_prefix_index = bowtie2_index_output_prefix
bowtie2_mapping_options =  config["bowtie2_mapping"]["options"]
final_output.extend(expand(bowtie2_mapping_sort, SAMPLE=samples, REF=ref))
include: os.path.join(RULES, "bowtie2_mapping.rules")
    


#----------------------------------
# mCherry counting
#----------------------------------
if config["genome"]["host_mapping"]: 
    # counting on mCherry
    cherry_counting_input = expand("02-Mapping/{REF}/{SAMPLE}_{REF}_sort.bam", SAMPLE=samples, REF=config["genome"]["host_name"])
    cherry_counting_output = "mCherry_metrics.out"
    cherry_counting_log =  "02-Mapping/mCherry_metrics.out"
    final_output.extend([cherry_counting_output])
    include: os.path.join(RULES, "cherry_counting.rules")

#----------------------------------
# genome Coverage
#----------------------------------
    
# then we performed genome coverage only in files against reference genome and not host genome !
ref = config["genome"]["name"] 
   
# Genome coverage for plus and minus strand
strand = ["minus", "plus"]
genomecov_input = "02-Mapping/{}/{{SAMPLE}}_{}_sort.bam".format(ref, ref)
genomecov_logs = "03-Coverage/logs/{SAMPLE}-{STRAND}_coverage.out"
genomecov_output = "03-Coverage/{SAMPLE}-{STRAND}_coverage.csv"
genomecov_genome = config["genome"]["fasta_file"]
genomecov_options = "" 

final_output.extend(expand(genomecov_output, SAMPLE=samples, STRAND=strand))
include: os.path.join(RULES, "genomecov.rules")



#----------------------------------
# write target for Rscript
#----------------------------------

write_target_input = expand(genomecov_output, SAMPLE=samples, STRAND=strand)
write_target_design = config['design']['design_file']
write_target_outputFile = "config/design_auto.txt"    
final_output.extend([write_target_outputFile])
include: os.path.join(RULES, "write_target.rules")

#----------------------------------
# RNAsig R script
#----------------------------------

rnasig_target = write_target_outputFile
rnasig_output_dir = "04-RNAsig/"
rnasig_covdir = "03-Coverage/"
rnasig_gfffile = config['genome']['gff_file']
rnasig_report = "04-RNAsig/finalplot.pdf"
rnasig_logs_out = "04-RNAsig/logs/rnasig.out"
rnasig_logs_err = "04-RNAsig/logs/rnasig.err"

final_output.extend([rnasig_report])
include: os.path.join(RULES, "rnasig.rules")


#----------------------------------  
# MultiQC report
#----------------------------------
multiqc_input = final_output
multiqc_input_dir = "."
multiqc_logs = "05-Multiqc/multiqc.log"
multiqc_output = "05-Multiqc/multiqc_report.html"
multiqc_options = config['multiqc']['options'] + " -c config/multiqc_config.yaml" 
multiqc_output_dir = "05-Multiqc"
final_output = [multiqc_output]
include: os.path.join(RULES, "multiqc.rules")




rule all:
    input: [final_output, rnasig_report]
        

        

onsuccess:

    import os
    import glob
    import shutil
    # move cluster log files
    pattern = re.compile("slurm.*")
    dest = "cluster_logs"
    for filepath in os.listdir("."):
        if pattern.match(filepath):
            if not os.path.exists(dest):
                os.makedirs(dest)
            shutil.move(filepath, dest)



    
    

